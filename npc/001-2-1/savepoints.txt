// Evol scripts.
// Author:
//    Reid
// Description:
//    Free save point.

001-2-1,27,30,0	script	#savepoint0-001-2-1	NPC_SAVE_POINT,{
    savepointparticle .map$, .x, .y, CURRENT_INN;
    close;

OnInit:
    .distance = 2;
    .sex = G_OTHER;
    end;
}
